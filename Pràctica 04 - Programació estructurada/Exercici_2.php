<?php
# Gurjinder Singh Kaur
# Exercici 2 - Calcula el volum d’un con

function Càlcul_Volum($radi, $altura) #Creem la funció
{
    $formula = $radi * $radi * 3.14 * $altura * (1/3); #Aquesta seria la fórmula del càlcul del volum d’un con
    return $formula;
}

$Radi = readline("Quin és el radi? "); #Preguntem per quin radi volem calcular el con
$Altura = readline("Quina és la altura? "); #Preguntem per quina altura volem calcular el con
$area = Càlcul_Volum($Radi, $Altura);
echo 'El volum del con és: ' . $area . "\n";
?>