<?php
# Gurjinder Singh Kaur
# Exercici 3 - Matrius

  $matriu1 = array
  (
    array(1,4,7),
    array(2,5,8),
    array(3,6,9)
  );
  $matriu2 = array
  (
    array(9,6,3),
    array(8,5,2),
    array(7,4,1)
  );

  function matriu_multiplicacio($m1,$m2){
    $r=count($m1);
    $c=count($m2[0]);
    $p=count($m2);
    $m3=array();
    for ($i=0;$i< $r;$i++){
      for($j=0;$j<$c;$j++){
        $m3[$i][$j]=0;
        for($k=0;$k<$p;$k++){
          $m3[$i][$j]+=$m1[$i][$k]*$m2[$k][$j];
        }
      }
    }
    return($m3);
}

// Ara imprimim la matriu que és un array de la següent forma: 
// 1 2 3
// 4 5 6
// 7 8 9

function print_matriu($m){
  $r=count($m);
  $c=count($m[0]);
  for ($i=0;$i< $r;$i++){
    for($j=0;$j<$c;$j++){
      echo $m[$i][$j]." ";
    }
    echo " \n";
  }
}
  
print_matriu(matriu_multiplicacio($matriu1,$matriu2));
?>