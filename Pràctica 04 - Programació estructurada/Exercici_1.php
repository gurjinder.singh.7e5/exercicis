<?php
# Gurjinder Singh Kaur
# Exercici 1 - Dividir paraula entre vocals i consonants

$qualsevol_paraula = readline("Escriu una paraula qualsevol: ");
$llista_vocals = "";
$llista_consonants = "";
function separarVocalsDeConsonants ($qualsevol_paraula,&$llista_vocals,&$llista_consonants){
    for ($i=0;$i<strlen($qualsevol_paraula);$i++)
	{
        if (vocal($qualsevol_paraula[$i]) === true){
            $llista_vocals = $llista_vocals . $qualsevol_paraula[$i];
        }
        else {
            $llista_consonants = $llista_consonants . $qualsevol_paraula[$i];
        }
    }
    return $llista_vocals . $llista_consonants;
}
function vocal ($lletra)
{
    if($lletra === "a" || $lletra === "e" || $lletra === "i" || $lletra === "o" || $lletra === "u"){ 
        return true;
    } 
    else {
        return false;
    }
}

echo separarVocalsDeConsonants($qualsevol_paraula,$llista_vocals,$llista_consonants, "/n");
?>