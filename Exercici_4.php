<?php
// Gurjinder Singh Kaur
// Exercici 4 - La setmana

$dia=date('D');

switch ($dia){
    case "Mon":
        echo "El dia de la setmana és: Dilluns \n";
        break;

    case "Tue":
        echo "El dia de la setmana és: Dimarts \n";
        break;

    case "Wed":
        echo "El dia de la setmana és: Dimecres \n";
        break;

    case "Thu":
        echo "El dia de la setmana és: Dijous \n";
        break;

    case "Fri":
        echo "El dia de la setmana és: Divendres \n";
        break;

    case "Sat":
        echo "El dia de la setmana és: Dissabte \n";
        break;

    case "Sun":
        echo "El dia de la setmana és: Diumenge \n";
        break;
}
?>