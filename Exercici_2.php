<!DOCTYPE html>
<html>
<body>
<style>
td {
  border: 1px solid red;
  border-collapse: collapse;
}
</style>
<h2>Tabulant informació</h2>

<table>
<tr>
<th>Índex</th>
<th>Valor</th>
</tr>

<?php
// Gurjinder Singh Kaur
// Exercici 2 - Tabulant informació

$matriz[0] = "cougar";
$matriz[1] = "ford";
$matriz[2] = null;
$matriz[3] = "2.500";
$matriz[4] = "V6";
$matriz[5] = 182;

$fields = count($matriz);
$cont = 1;

while ($cont < $fields) {
    echo "<tr>";
    echo "<td>" . $cont . "</td>";
    echo "<td>" . $matriz[$cont] . "</td>";
    echo "</tr>";
    $cont++;
}
?>

</table>
</body>
</html>